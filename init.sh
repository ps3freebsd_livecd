#!/bin/sh

#set -x

PATH=/rescue

BASEROOT=/baseroot
RWROOT=/rwroot
CDROM=/cdrom
DEVFS=$BASEROOT/dev
ROOT_IMAGE=/data/root.ufs.uzip

mount -u -w /

mkdir -p $BASEROOT
mkdir -p $RWROOT
mkdir -p $CDROM

mount -t cd9660 /dev/cd0 $CDROM

mdmfs -P -F $CDROM/$ROOT_IMAGE -o ro md.uzip $BASEROOT
mdmfs -s 16m md $RWROOT
mount -t unionfs $RWROOT $BASEROOT

mkdir -p $DEVFS
mount -t devfs devfs $DEVFS

mkdir -p $BASEROOT/$CDROM
mount -t nullfs -o ro $CDROM $BASEROOT/$CDROM

kenv init_shell="/bin/sh"

exit 0
